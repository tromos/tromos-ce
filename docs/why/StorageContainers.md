Storage Containers
==================

A Storage container is a persistent storage middleware  that decouples application I/O logic from functionality logic.
It allows developers to apply their I/O related policies without intervention in the source
code.  Being distributed, it can cluster clients to form a virtual storage infrastructure for
a group of related applications.  It is not meant to replace existing general purpose data-
stores,  but provide a data management layer that insulates application and allows it to
scale independently.  In the conventional notion, storage is the asset where applications
come and go. Thereby, it is justified to apply one-size-fit-them-all policies to all the appli-
cations. ATS leans toward “application is the asset and can use any combination of existing
storage systems”. When the application terminates, so does the ATS. It allows developers
to customize namespace management, consistency level, workload distribution, crash re-
siliency, and many others, on a per-application basis. Bellow, we present a list of use cases
where ATS can be a valuable asset


Portability
-----------
Claiming application portability across platforms entails predictable behavior across all
the platform choices.  Two distinct aspects often conflated are the API and the protocol.
The API is a shared boundary across which two or more separate components exchange
information. The protocol is a contract between components that define what is and is not
guaranteed to happen on an API call. It defines the rules, syntax, semantics, synchroniza-
tion of communication and possible error recovery methods.  It does not define though
how to realize them, i.e., implementation details. Along these lines, API is about portabil-
ity on the source-code level whereas protocol is about portability on the binary-level.
Due to high synchronization overheads, filesystems in High-Performance Computing
(HPC) tend to replace the strong Portable Operating System Interface (POSIX) semantics,
with more relaxed semantics that favor scalability and concurrency. They are still accessi-
ble though through the same POSIX API. Thereby, two filesystems that expose the same
API do not necessarily implement the same semantics [51].
The same holds for multi-cloud libraries [58, 65, 117].  They unify Cloud Storage pro-
vides on the API level, but not on the semantics.  For API, unification means integration
of the lowest common denominator of all vendor-specific API, i.e., the calls that are sup-
ported by all platforms.  For protocol, unification means integration to the stricter of all
vendor-specific protocols. From an application perspective, providing semantics stronger
than those requested impose unnecessary synchronization overheads. Otherwise, seman-
tics weaker than the requested can jeopardize application correctness [50, 51, 96, 110, 119,
126].  Evidently, the most appropriate "reference" protocol for an application depends on
the application itself, and therefore general purpose systems, or libraries, cannot provide
it, if they want to remain generic
