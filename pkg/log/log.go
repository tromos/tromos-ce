// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package log // import "gitlab.com/tromos/tromos-ce/pkg/log"

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
	"strings"
)

var ShowAdmin = true
var ShowUser = true
var ShowTrace = false

var logger = logrus.New()

// Fields wraps logrus.Fields, which is a map[string]interface{}
type Fields logrus.Fields

func init() {
	//logger.Formatter = new(logrus.JSONFormatter)
	logger.Formatter = new(logrus.TextFormatter)
	logger.Level = logrus.DebugLevel
	logrus.SetOutput(os.Stdout)
}

func FileInfo(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if !ok {
		file = "<???>"
		line = 1
	} else {
		slash := strings.LastIndex(file, " tromos")
		if slash >= 0 {
			file = file[slash+1:]
		}
	}
	return fmt.Sprintf("%s:%d", file, line)
}

/* Issues related to the administrator. This may include
 * connection errors, bad lookups ... */
func Admin(args ...interface{}) {
	entry := logger.WithFields(logrus.Fields{})
	entry.Data["file"] = FileInfo(2)
	entry.Error(args...)
}

/* Issues related to the User. This may include
 * who started a transaction, when, how many data ... */
func User(args ...interface{}) {
	entry := logger.WithFields(logrus.Fields{})
	entry.Data["file"] = FileInfo(2)
	entry.Info(args...)
}

/* Whatever doesn't match the previous categories
 * goes here */
func Trace(args ...interface{}) {

}
