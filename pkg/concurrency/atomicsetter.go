// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package concurrency // import "gitlab.com/tromos/tromos-ce/pkg/concurrency"

import "sync"

func NewAtomicSetter() *AtomicSetter {
	return &AtomicSetter{
		finalCh: make(chan struct{}),
	}
}

type AtomicSetter struct {
	locker  sync.Mutex
	final   bool
	finalCh chan struct{}
}

func (a *AtomicSetter) IsFinal() bool {
	a.locker.Lock()
	defer a.locker.Unlock()

	return a.isFinal()
}

func (a *AtomicSetter) isFinal() bool {
	select {
	case <-a.finalCh:
		return true
	default:
		return false
	}

}

func (a *AtomicSetter) Set(protected func()) {
	a.locker.Lock()
	defer a.locker.Unlock()

	if a.isFinal() {
		panic("Setting value on finalized context")
	}
	protected()
}

func (a *AtomicSetter) Finalize(protected func()) {

	a.locker.Lock()
	defer a.locker.Unlock()

	if a.final {
		panic("Value has been already finalized")
	}

	if protected != nil {
		protected()
	}

	a.final = true
	defer close(a.finalCh)
}

func (a *AtomicSetter) WaitFinal() {
	<-a.finalCh
}
