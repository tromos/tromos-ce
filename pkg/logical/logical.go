// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package logical // import "gitlab.com/tromos/tromos-ce/pkg/logical"

import (
	"gitlab.com/tromos/hub/logical"
)

type DefaultLogical struct {
	length uint64
}

func (df *DefaultLogical) Add(offset int64, size int64) uint64 {
	if uint64(size) > df.length {
		df.length = uint64(size)
	}
	return 0
}

func (df *DefaultLogical) Overlaps(offset int64, bytes int64) (size uint64, segments []logical.Segment) {

	return df.length, []logical.Segment{{To: int64(df.length)}}
}

func (df *DefaultLogical) Length() int64 {
	return int64(df.length)
}
