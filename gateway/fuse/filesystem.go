// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
)

var storage *middleware.Client

type FilesystemConfig struct {
	Conn   *fuse.Conn
	Client *middleware.Client
}

func Mount(conf FilesystemConfig) error {

	storage = conf.Client

	if p := conf.Conn.Protocol(); !p.HasInvalidate() {
		panic("Kernel fuse support is too old to have invalidation")
	}

	srv := fs.New(conf.Conn, &fs.Config{
		/*
			Debug: func(msg interface{}) {
				log.Admin(msg)
			},
		*/
	})

	return srv.Serve(&Filesystem{
		RootNode: &Directory{
			name:     "root",
			children: make(map[string]interface{}),
		},
	})
}

type Filesystem struct {
	RootNode *Directory
}

// To be used by the main.go of the library
func (fs *Filesystem) Root() (fs.Node, error) {
	return fs.RootNode, nil
}
