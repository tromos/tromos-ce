// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package devicemanager // import "gitlab.com/tromos/tromos-ce/engine/middleware/devicemanager"

import (
	"net"
	"sync"

	//"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gopkg.in/go-playground/validator.v9"
	//rpc "github.com/hprose/hprose-golang/rpc/websocket"
	//"gitlab.com/tromos/tromos-ce/pkg/log"
)

// Config includes the execution paramers for the DeviceManager
type Config struct {
	//	WebService string            `validate:"isdefault|ip"`
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

// DeviceManager provides clustered access to the Coordinators
type DeviceManager struct {
	config Config

	reservationLocker sync.Mutex
	reservations      map[string][]string // request:exludeList

	closed bool
	//webservice *http.Server
	//authorities map[string]*authorityClient
}

var validate *validator.Validate = validator.New()

// New instantiates a new DeviceManager
func New(config Config) (*DeviceManager, error) {

	if config == (Config{}) {
		return nil, ErrInvalid
	}

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Error("Cannot create new DeviceManager")
		return nil, ErrInvalid
	}

	dm := &DeviceManager{
		config:       config,
		reservations: make(map[string][]string),
		//authorities:  make(map[string]*authorityClient),
	}

	for _, name := range config.Peer.Devices() {
		dev, ok := config.Peer.Device(name)
		if !ok {
			panic("This should neven happen")
		}

		dm.config.Selector.Add(selector.Properties{
			ID:           dev.String(),
			Capabilities: dev.Capabilities(),
			Peer:         dev.Location(),
		})
	}
	dm.config.Selector.Commit()
	/*
		// Optionally make the query functions of the device manager accessible through the network
		// This is need when the Authority Device Manager is other than the insitu Device Manager
		// (the case for datapaths that span across several nodes)
		if dm.config.WebService == "" {
			ops := rpc.NewWebSocketService()
			ops.AddFunction("ProxySelectAndReserve", dm.SelectAndReserve)
			webservice := &http.Server{
				Addr:    dm.config.WebService,
				Handler: ops,
			}
			dm.webservice = webservice

			go func() {
				if err := webservice.ListenAndServe(); err != nil {
					panic(err)
				}
			}()
		}
	*/
	return dm, nil
}

// Close closes the device manager
func (dm *DeviceManager) Close() error {
	if dm.closed {
		return ErrClosed
	}
	dm.closed = true

	return nil
	//	return dm.webservice.Shutdown(context.TODO())
}

// Reserve asks the Authority Device manager to select and reserve
// one of the available Devices in the cluster. It returns a connector to the selected Device
func (dm *DeviceManager) Reserve(authority net.IP, tid string, capabilities ...selector.Capability) (device.Device, error) {
	if dm.closed {
		return nil, ErrClosed
	}

	if authority == nil || tid == "" {
		return nil, ErrInvalid
	}

	// If the indicatd webservice is running locally, do not go through the network
	if authority.IsLoopback() {
		deviceID, err := dm.SelectAndReserve(tid, capabilities...)
		if err != nil {
			return nil, err
		}
		return dm.GetDevice(deviceID)
	} /*
		else {
			client := dm.getAuthorityClient(authority)
			deviceID, err = client.call.SelectAndReserve(tid, capabilities...)
			if err != nil {
				return nil, err
			}
		}
	*/
	panic("Should not go here")
}

/*
type authorityClient struct {
	conn *rpc.WebSocketClient
	call *WebServiceOperations
}

func (dm *DeviceManager) getAuthorityClient(service string) *authorityClient {

	// reuse client from a pool, if the connection to authority device manager
	// already exists
	cli, ok := dm.authorities[service]
	if ok {
		return cli
	}

	call := &WebServiceOperations{}
	conn := rpc.NewWebSocketClient("ws://" + service + "/")
	//client.SetTimeout(cli.timeout)
	conn.UseService(call)

	return &authorityClient{call: call, conn: conn}
}
*/
// SelectAndReserve selects and reserves one of the available Devices in the cluster based
// on the capability contrains. It returns the Device ID
func (dm *DeviceManager) SelectAndReserve(tid string, capabilities ...selector.Capability) (string, error) {
	if dm.closed {
		return "", ErrClosed
	}

	if tid == "" {
		return "", ErrInvalid
	}

	dm.reservationLocker.Lock()
	defer dm.reservationLocker.Unlock()

	reserved := dm.reservations[tid]

	selected, err := dm.config.Selector.Select(reserved, capabilities...)
	if err != nil {
		return "", err
	}

	// Exclude from the selection any device that is already selected
	// for the current transaction
	reserved = append(reserved, selected)
	dm.reservations[tid] = reserved

	return selected, nil
}

// GetDevice returns a connector to the Device identified by deviceID
func (dm *DeviceManager) GetDevice(devid string) (device.Device, error) {
	if dm.closed {
		return nil, ErrClosed
	}

	if devid == "" {
		return nil, ErrInvalid
	}

	d, ok := dm.config.Peer.Device(devid)
	if !ok {
		return nil, ErrNotFound
	}
	return d, nil
}
