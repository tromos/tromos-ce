// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package devicemanager // import "gitlab.com/tromos/tromos-ce/engine/middleware/devicemanager"

import (
	"net"
	"sync"
	"testing"

	//"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/selector"
	randomsel "gitlab.com/tromos/hub/selector/random/lib"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
)

func defaultConfig() Config {

	man, err := manifest.New(manifest.DefaultConfig(1, 0, 0))
	if err != nil {
		panic(err)
	}

	p, err := peer.New(peer.Config{Manifest: man})
	if err != nil {
		panic(err)
	}

	return Config{
		Peer:     p,
		Selector: randomsel.New(nil),
	}
}

func TestNew(t *testing.T) {
	var err error

	_, err = New(Config{})
	assert.NotNil(t, err) // Validator error

	_, err = New(Config{
		Peer:     nil,
		Selector: randomsel.New(nil),
	})
	assert.NotNil(t, err) // Validator error

	dm, err := New(DefaultConfig())
	assert.Nil(t, err)

	err = dm.Close()
	assert.Nil(t, err)

	err = dm.Close()
	assert.Equal(t, err, ErrClosed, "DeviceManager is already closed")
}

func TestReserve(t *testing.T) {
	dm, err := New(DefaultConfig())
	assert.Nil(t, err)

	_, err = dm.Reserve(nil, "")
	assert.Equal(t, err, ErrInvalid, "Empty arguments")

	_, err = dm.Reserve(net.ParseIP("127.0.0.1"), "")
	assert.Equal(t, err, ErrInvalid, "Empty arguments")

	_, err = dm.Reserve(nil, uuid.Once())
	assert.Equal(t, err, ErrInvalid, "Empty arguments")

	dev, err := dm.Reserve(net.ParseIP("127.0.0.1"), uuid.Once())
	assert.Nil(t, err)
	assert.NotNil(t, dev)
}

func TestSelectAndReserve(t *testing.T) {
	dm, err := New(DefaultConfig())
	assert.Nil(t, err)

	_, err = dm.SelectAndReserve("")
	assert.Equal(t, err, ErrInvalid, "Empty arguments")

	_, err = dm.SelectAndReserve(uuid.Once())
	assert.Nil(t, err)

	for i := 0; i < 5; i++ {
		dev, err := dm.SelectAndReserve(uuid.Once(), selector.Default)
		assert.Nil(t, err)
		assert.NotNil(t, dev)
	}

	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			dev, err := dm.SelectAndReserve(uuid.Once(), selector.Default)
			assert.Nil(t, err)
			assert.NotNil(t, dev)
		}()
	}
	wg.Wait()
}

func TestGetDevice(t *testing.T) {
	dm, err := New(DefaultConfig())
	assert.Nil(t, err)

	_, err = dm.GetDevice("")
	assert.Equal(t, err, ErrInvalid, "Empty arguments")

	_, err = dm.GetDevice(uuid.Once())
	assert.Equal(t, err, ErrNotFound, "Unregistered Device")

	// default
	_, err = dm.GetDevice("default0")
	assert.Nil(t, err)

	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			dev, err := dm.GetDevice("default0")
			assert.Nil(t, err)
			assert.NotNil(t, dev)
		}()
	}
	wg.Wait()
}
