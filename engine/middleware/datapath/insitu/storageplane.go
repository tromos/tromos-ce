// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package insitu // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"

import (
	"github.com/sirupsen/logrus"
	flow "github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	//"io"
	//"io/ioutil"
	//"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
)

type transfer struct {
	in  processor.Stream
	out *device.Stream
}

type storagePlane struct {
	flow.Component
	logger *logrus.Entry

	In  <-chan discover
	Out chan<- discover
}

func newStoragePlane() *storagePlane {
	return &storagePlane{
		logger: logrus.WithField("Datapath", "Storageplane"),
	}
}

func (plane *storagePlane) OnIn(req discover) {
	if req.Request.Writable {
		plane.Upstream(req)
	} else {
		plane.Downstream(req)
	}
}

func (plane *storagePlane) Upstream(req discover) {
	// Select a device
	dev, err := req.Request.DeviceManager.Reserve(
		req.Request.Authority,
		req.Request.Name,
		req.Request.Capabilities...,
	)
	if err != nil {
		plane.logger.Printf("Channel establishment %s failed. Msg: %s",
			req.Request.Name, err)

		req.Response <- err
		close(req.Response)
		return
	}

	// Create a device channel
	ch, err := dev.NewWriteChannel(req.Request.Name)
	if err != nil {
		plane.logger.Printf("Channel establishment %s failed. Msg: %s",
			req.Request.Name, err)

		req.Response <- err
		close(req.Response)
		return
	}

	// Acknowledge reservation to the higher layers
	reservation := reservation{
		Listener: make(chan processor.Stream),
		Alive:    make(chan error),
	}
	req.Response <- reservation
	close(req.Response)

	// Some logging
	plane.logger.Printf("Channel %s established. Writable: %t",
		req.Request.Name, req.Request.Writable)
	defer plane.logger.Printf("Channel %s released. Writable: %t",
		req.Request.Name, req.Request.Writable)

	// Serve streams of the channel
	var transfers []*transfer
	for loop := true; loop; {
		select {
		case <-req.Request.Context.Done():
			plane.logger.Print("Context stopped")
			loop = false

		case in, ok := <-reservation.Listener:
			if !ok {
				plane.logger.Print("Listener stopped")
				loop = false
				continue
			}

			transfer := &transfer{
				in:  in,
				out: &device.Stream{Complete: make(chan struct{})},
			}

			reader := transfer.in.Data.(processor.Receiver).PipeReader
			if err := ch.NewTransfer(reader, transfer.out); err != nil {
				plane.logger.Error("Error Occurred:", err)
				loop = false
				reservation.Alive <- err
				continue
			}
			transfers = append(transfers, transfer)
		}
	}
	// Terminate channel with the device
	if err := ch.Close(); err != nil {
		reservation.Alive <- err
		close(reservation.Alive)
	}

	// Fix metadata
	for _, transfer := range transfers {
		transfer.in.Meta.SetItem(req.WalkID(), transfer.out.Item)

		plane.logger.Printf("Stream [%s] -> [%s] wrote %d bytes",
			req.WalkID(), dev.String(), transfer.out.Item.Length())
	}
	req.Request.Sinks[req.WalkID()] = dev.String()

	// Terminate channel with upper layer
	close(reservation.Alive)
}

func (plane *storagePlane) Downstream(req discover) {

	deviceID := req.Request.Sinks[req.WalkID()]
	if deviceID == "" {
		panic("Perhaps you forgot to define sink")
	}

	dev, err := req.Request.DeviceManager.GetDevice(deviceID)
	if err != nil {
		plane.logger.Printf("Channel establishment %s failed (GetDevice). Msg: %s",
			req.Request.Name, err)
		req.Response <- err
		close(req.Response)
		return
	}

	ch, err := dev.NewReadChannel(req.Request.Name)
	if err != nil {
		plane.logger.Printf("Channel establishment %s failed (NewReadChannel). Msg: %s",
			req.Request.Name, err)

		req.Response <- err
		close(req.Response)
		return
	}

	reservation := reservation{
		Listener: make(chan processor.Stream),
		Alive:    make(chan error),
	}
	req.Response <- reservation
	close(req.Response)

	plane.logger.Printf("Channel %s established. Writable: %t",
		req.Request.Name, req.Request.Writable)
	defer plane.logger.Printf("Channel %s released. Writable: %t",
		req.Request.Name, req.Request.Writable)

	//	var transfers []*transfer
	for loop := true; loop; {
		select {
		case <-req.Request.Context.Done():
			loop = false

		case in, ok := <-reservation.Listener:
			if !ok {
				loop = false
				continue
			}

			// It is possible that data are contained only in a subset of the devices.
			// This can be either due to scan where data are located only
			// on a specific device and the rest are nil, or like strip
			// where dataholders have been created but not populated with data
			item, ok := in.Meta.GetItem(req.WalkID())
			if !ok || item.IsEmpty() {
				// Terminate the writer. No need to go further into the storage line
				if err := in.Data.Close(); err != nil {
					panic(err)
				}
			} else {
				writer := in.Data.(processor.Sender).PipeWriter
				err := ch.NewTransfer(writer, &device.Stream{Item: item})
				if err != nil {
					panic(err)
				}
			}
		}
	}

	// Terminate channel with the device
	if err := ch.Close(); err != nil {
		reservation.Alive <- err
		close(reservation.Alive)
		return
	}

	// Terminate channel with upper layer
	close(reservation.Alive)

}
