// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package insitu // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"

import (
	"net"
	"strings"

	"github.com/sirupsen/logrus"
	flow "github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/engine/middleware/devicemanager"
)

type channelConfig struct {
	processor.ChannelConfig
	Authority     net.IP
	DeviceManager *devicemanager.DeviceManager
}

type reservation struct {
	Listener chan processor.Stream // Closing listener : top-bottom
	Alive    chan error            // Closing report: bottom-up
}

type discover struct {
	Request  channelConfig
	Response chan interface{}
}

func (req *discover) WalkID() string {
	if req.Request.WalkID == nil {
		panic("Empty WalkID is not allowed")
	}
	return strings.Join(req.Request.WalkID, ".")
}

func discoverNext(nexthop chan<- discover, config channelConfig) (reservation, error) {

	// forward the request and wait for response
	req := discover{
		Request:  config,
		Response: make(chan interface{}),
	}
	nexthop <- req
	resp := <-req.Response
	switch v := resp.(type) {
	case error:
		return reservation{}, v
	case reservation:
		return v, nil
	default:
		panic("Unknown datapath response type")
	}

}

type processPlane struct {
	flow.Component

	In  <-chan discover
	Out chan<- discover

	proc   processor.Processor
	logger *logrus.Entry
}

func newProcessPlane(proc processor.Processor) *processPlane {
	return &processPlane{
		proc:   proc,
		logger: logrus.WithField("Datapath", "processPlane"),
	}
}

func (plane *processPlane) OnIn(req discover) {
	plane.newreservation(req)
}

func (plane *processPlane) newreservation(req discover) {

	nextHops := make(map[string]*channel)
	req.Request.Pushout = func(stream processor.Stream) {
		if err := nextHops[stream.Port].NewTransfer(stream); err != nil {
			panic(err)
		}
	}

	// Reserve channel on the local processor
	ch, err := plane.proc.NewChannel(req.Request.ChannelConfig)
	if err != nil {
		req.Response <- err
		close(req.Response)
		return
	}

	// Link with DAG outputs (populate nextHops)
	for port := range ch.Sinks() {
		lowerChConfig := channelConfig{
			Authority:     req.Request.Authority,
			DeviceManager: req.Request.DeviceManager,
			ChannelConfig: processor.ChannelConfig{
				Context:  req.Request.Context,
				Writable: req.Request.Writable,
				Name:     req.Request.Name,
				Sinks:    req.Request.Sinks,
				WalkID:   append(req.Request.WalkID, port),
				Capabilities: append(req.Request.Capabilities,
					ch.Capabilities(port)...),
				Pushout: nil,
			},
		}

		reservation, err := discoverNext(plane.Out, lowerChConfig)
		if err != nil {
			// TODO: Cancel all reservation is one fails
			req.Response <- err
			close(req.Response)
			return
		}

		nextHops[port] = &channel{
			config:   lowerChConfig.ChannelConfig,
			response: reservation,
		}
	}

	// Affirm link with upper layer
	reservation := reservation{
		Listener: make(chan processor.Stream),
		Alive:    make(chan error),
	}
	req.Response <- reservation
	close(req.Response)

	plane.logger.Printf("Channel %s established. Writable: %t",
		req.Request.Name, req.Request.Writable)
	defer plane.logger.Printf("Channel %s released. Writable: %t",
		req.Request.Name, req.Request.Writable)

	// Loop waiting for events from upper layers. Loop stops when the listener
	// is closed or when the context is cancelled
	for loop := true; loop; {
		select {
		case <-req.Request.Context.Done():
			plane.logger.Print("Context stopped")
			loop = false
		case in, ok := <-reservation.Listener:
			if !ok {
				loop = false
				plane.logger.Print("Listener stopped")
				continue
			}
			// Start async I/O with the local processor
			if err := ch.NewTransfer(in); err != nil {
				plane.logger.Error("LALA:", err)
				reservation.Alive <- err
				loop = false
			}

			/*
				// Write the outputs of the stream (process by the graph) to the next hops.
				// A trick is needed here for termination after all the ports of the graph are writte.
				// This is because the Readyports is unbounded and loops waiting for a signal to come
				completed := 0
				for substream := range ch.Readyports() {

					nextHops[substream.Port].NewTransfer(substream)
					completed++

					if completed == len(nextHops) {
						break
					}
				}
			*/
		}
	}

	plane.logger.Print("Close local processor")
	// Terminate channel with local processor (flush existing requests)
	if err := ch.Close(); err != nil {
		reservation.Alive <- err
		close(reservation.Alive)
	}

	plane.logger.Print("Close next hops:", nextHops)
	//	Terminate channels with lower layers (stop accepting requests)
	for _, nexthop := range nextHops {
		if err := nexthop.Close(); err != nil {
			panic(err)
		}
	}

	plane.logger.Print("Acknowledge termination")
	// Terminate channel with upper layer (acknowledge termination)
	close(reservation.Alive)
}
