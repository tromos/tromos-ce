// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package insitu // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"

import (
	"context"
	"net"
	"sync"

	"github.com/sirupsen/logrus"
	flow "github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	randomsel "gitlab.com/tromos/hub/selector/random/lib"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/middleware/devicemanager"
	"gitlab.com/tromos/tromos-ce/engine/middleware/processormanager"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gopkg.in/go-playground/validator.v9"
)

// DefaultConfig return the configuration for a local Datapath with 10 devices. If
// complex is set to false, only direct graph will be used
func DefaultConfig(complex bool) Config {

	man, err := manifest.New(manifest.DefaultConfig(1, 1, 0))
	if err != nil {
		panic(err)
	}

	p, err := peer.New(peer.Config{Manifest: man})
	if err != nil {
		panic(err)
	}

	pm, err := processormanager.New(processormanager.Config{
		Peer:     p,
		Selector: randomsel.New(nil),
	})
	if err != nil {
		panic(err)
	}

	dm, err := devicemanager.New(devicemanager.Config{
		Peer:     p,
		Selector: randomsel.New(nil),
	})
	if err != nil {
		panic(err)
	}

	return Config{
		ProcessorManager: pm,
		DeviceManager:    dm,
	}
}

// Config defines the running paramers of the Device
type Config struct {
	ProcessorManager *processormanager.ProcessorManager `validate:"required"`
	DeviceManager    *devicemanager.DeviceManager       `validate:"required"`
}

var _ processor.Processor = (*LocalDatapath)(nil)
var validate *validator.Validate = validator.New()

// LocalDatapath connect insitu processor with insitu device manager
type LocalDatapath struct {
	config Config

	discover chan discover
	network  *flow.Graph
	closed   bool

	// Context is used to forcibly cancel all pending channels
	ctx    context.Context
	cancel context.CancelFunc

	logger *logrus.Entry
}

// NewDatapath returns a new LocalDatapath compliant with processor.Processor
func NewDatapath(config Config) (*LocalDatapath, error) {
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	// Associate Datapath to a Processor instance. Datapath channels
	// trigger the respective Processor channels
	proc, err := config.ProcessorManager.SelectAndReserve()
	if err != nil {
		return nil, err
	}

	// Graph constructor and structure definition of IO network
	// creates the object in heap
	var network *flow.Graph = new(flow.Graph)
	{
		// allocates memory for the graph
		network.InitGraphState()
		// Add processes to the network
		network.Add(newProcessPlane(proc), "InTransit")
		network.Add(newStoragePlane(), "Storage")
		// Connect them with a channel
		network.Connect("InTransit", "Out", "Storage", "In")
		// Our net has 1 inport mapped to network.Name
		network.MapInPort("In", "InTransit", "In")
	}

	// We need a channel to talk to the network
	discover := make(chan discover)
	network.SetInPort("In", discover)

	// Run the net
	flow.RunNet(network)
	// Block until the runtime is ready to work
	<-network.Ready()

	ctx, cancel := context.WithCancel(context.Background())

	return &LocalDatapath{
		config:   config,
		discover: discover,
		network:  network,
		ctx:      ctx,
		cancel:   cancel,
		logger:   logrus.WithField("Datapath", "Insitu"),
	}, nil
}

// String returns a descriptive name for the Processor
func (path *LocalDatapath) String() string {
	if path.closed {
		panic("Datapath is closed")
	}
	return "DefaultDatapath"
}

// Location returns the node where the Processor is running
func (path *LocalDatapath) Location() string {
	if path.closed {
		panic("Datapath is closed")
	}
	return ""
}

// Capabilities returns the capabilities of the Datapath
func (path *LocalDatapath) Capabilities() []selector.Capability {
	if path.closed {
		panic("Datapath is closed")
	}
	return nil
}

// NewChannel returns a new channel associated to the Processor
func (path *LocalDatapath) NewChannel(config processor.ChannelConfig) (processor.Channel, error) {
	if path.closed {
		panic(processor.ErrClosed)
	}

	// If validation finds no error, eventually it can be replaced
	// by this filling method
	config.FillMissingFields()

	// Validate channel configuration
	if err := validate.Struct(config); err != nil {
		path.logger.Error("POUUUUUTSES")
		path.logger.Error("Err:", err)
		return nil, processor.ErrInvalid
	}

	// TODO: if ever migrate to distributedDatapath, do not forget to set the
	// Authority Device Manager to the insitu device manager of the client
	// who initiates the channel (or to the central Authority Device Manager, if any)
	// To do so, use the cluster-facing ip into the WalkID
	conf := channelConfig{
		DeviceManager: path.config.DeviceManager,
		Authority:     net.ParseIP("127.0.0.1"),
		ChannelConfig: config,
	}

	reservation, err := discoverNext(path.discover, conf)
	if err != nil {
		return nil, err
	}

	return &channel{
		config:   config,
		response: reservation,
		logger:   logrus.WithField("Channel", config.Name),
	}, nil
}

// Close gracefully shutdowns the datapath
func (path *LocalDatapath) Close() error {
	if path.closed {
		return processor.ErrClosed
	}
	path.closed = true

	// Propagate cancellation to all pending channels
	path.cancel()

	// Tell the runtime to shut down
	path.network.Stop()

	// Close the input to shut the network down
	close(path.discover)

	// Wait until the app has done its job
	<-path.network.Wait()
	return nil
}

type channel struct {
	locker sync.Mutex
	closed bool

	config   processor.ChannelConfig
	response reservation

	logger *logrus.Entry
}

func (ch *channel) Config() processor.ChannelConfig {
	return ch.config
}

func (ch *channel) Capabilities(port string) []selector.Capability {
	return nil
}

func (ch *channel) NewTransfer(stream processor.Stream) error {

	// Prevent NewTransfer() while closing
	ch.locker.Lock()
	defer ch.locker.Unlock()

	if ch.closed {
		panic(processor.ErrClosed)
	}

	// Validate stream input
	if err := validate.Struct(stream); err != nil {
		ch.logger.Error("Err:", err)
		return processor.ErrInvalid
	}

	ch.response.Listener <- stream
	return nil
}

func (ch *channel) Sinks() map[string]string {
	if ch.closed {
		panic(processor.ErrClosed)
	}

	// TODO Find another way to get the sinks
	return ch.config.Sinks
}

func (ch *channel) Close() error {
	// Prevent NewTransfer() while closing
	ch.locker.Lock()
	defer ch.locker.Unlock()

	if ch.closed {
		return processor.ErrClosed
	}
	ch.closed = true

	// Propagate termination to lower layers
	close(ch.response.Listener)
	<-ch.response.Alive
	return nil
}
