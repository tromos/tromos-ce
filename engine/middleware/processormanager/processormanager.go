// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processormanager // import "gitlab.com/tromos/tromos-ce/engine/middleware/processormanager"

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gopkg.in/go-playground/validator.v9"
)

// Config includes the execution paramers for the ProcessorManager
type Config struct {
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

// ProcessorManager provides clustered access to the Processors
type ProcessorManager struct {
	config Config
	closed bool
}

var validate *validator.Validate = validator.New()

func New(config Config) (*ProcessorManager, error) {

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Print("Cannot create new ProcessorManager")
		return nil, err
	}

	pm := &ProcessorManager{
		config: config,
	}

	for _, name := range config.Peer.Processors() {
		proc, ok := config.Peer.Processor(name)
		if !ok {
			panic("This should never happen")
		}

		pm.config.Selector.Add(selector.Properties{
			ID:           proc.String(),
			Capabilities: proc.Capabilities(),
			Peer:         proc.Location(),
		})
	}
	pm.config.Selector.Commit()

	return pm, nil
}

// Close closes the device manager
func (pm *ProcessorManager) Close() error {
	if pm.closed {
		return ErrClosed
	}
	pm.closed = true

	return nil
}

// SelectAndReserve selects and reserves one of the available Processors in the cluster based
// on the capability contrains. It returns the Processor ID
func (pm *ProcessorManager) SelectAndReserve(capabilities ...selector.Capability) (processor.Processor, error) {
	if pm.closed {
		return nil, ErrClosed
	}

	procid, err := pm.config.Selector.Select(nil, capabilities...)
	if err != nil {
		return nil, err
	}
	return pm.GetProcessor(procid)
}

// GetProcessor returns a connector to the Processor identified by processorID
func (pm *ProcessorManager) GetProcessor(processorID string) (processor.Processor, error) {
	if pm.closed {
		return nil, ErrClosed
	}

	if processorID == "" {
		return nil, ErrInvalid
	}

	proc, ok := pm.config.Peer.Processor(processorID)
	if !ok {
		return nil, ErrNotFound
	}
	return proc, nil
}
