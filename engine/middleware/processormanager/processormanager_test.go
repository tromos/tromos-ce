// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processormanager // import "gitlab.com/tromos/tromos-ce/engine/middleware/processormanager"

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	randomsel "gitlab.com/tromos/hub/selector/random/lib"
	selector "gitlab.com/tromos/hub/selector/random/lib"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	//"github.com/sirupsen/logrus"
)

func defaultConfig() Config {
	man, err := manifest.New(manifest.DefaultConfig(0, 1, 0))
	if err != nil {
		panic(err)
	}

	p, err := peer.New(peer.Config{Manifest: man})
	if err != nil {
		panic(err)
	}

	return Config{
		Peer:     p,
		Selector: randomsel.New(nil),
	}
}

func TestNew(t *testing.T) {
	var err error

	_, err = New(Config{})
	assert.NotNil(t, err) // Validator error

	_, err = New(Config{
		Peer:     nil,
		Selector: selector.New(nil),
	})
	assert.NotNil(t, err) // Validator error

	pm, err := New(defaultConfig())
	assert.Nil(t, err)

	err = pm.Close()
	assert.Nil(t, err)

	err = pm.Close()
	assert.Equal(t, err, ErrClosed, "ProcessorManager is already closed")
}

func TestSelectAndReserve(t *testing.T) {
	pm, err := New(defaultConfig())
	assert.Nil(t, err)

	proc, err := pm.SelectAndReserve()
	assert.Nil(t, err)
	assert.NotNil(t, proc)

	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			proc, err := pm.SelectAndReserve()
			assert.Nil(t, err)
			assert.NotNil(t, proc)
		}()
	}
	wg.Wait()
}

func TestGetProcessor(t *testing.T) {
	pm, err := New(defaultConfig())
	assert.Nil(t, err)

	_, err = pm.GetProcessor("")
	assert.Equal(t, err, ErrInvalid, "Empty arguments")

	_, err = pm.GetProcessor(uuid.Once())
	assert.Equal(t, err, ErrNotFound, "Random device lookup")

	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			proc, err := pm.GetProcessor("default0")
			assert.Nil(t, err)
			assert.NotNil(t, proc)
		}()
	}
	wg.Wait()
}
