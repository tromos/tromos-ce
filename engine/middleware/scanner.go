// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

func (client *Client) ImportFrom(deviceID string) error {
	device, err := client.DeviceManager.GetDevice(deviceID)
	if err != nil {
		return err
	}

	files, items, err := device.Scan()
	if err != nil {
		return err
	}

	_ = files
	_ = items
	/*
		if len(items) == 0 {
			log.User("No items to index for resource:", deviceID)
			return nil
		}

		for i := 0; i < len(files); i++ {
			item := items[i]
			filename := files[i]

			partition := client.Namespace.Partition(filename)
			err := partition.CreateIfNotExist(filename)
			if err != nil {
				return err
			}

			// Use as default sink the "root.scannable"
			tx, err := client.BeginUpdate(filename, "", []processing.Sink{
				processing.Sink{WalkID: "root.scannable", DeviceID: deviceID},
			})
			if err != nil {
				return err
			}

			delta := tx.NewDelta(
				&processing.Stream{
					Items: map[string]data.Item{"root.scannable": item},
				})
			delta.Offset = 0
			delta.Size += int(item.Size)

			if err := client.EndUpdate(tx); err != nil {
				return err
			}
		}
		log.User("Indexed: ", len(items), " items for resource:", deviceID)
		return nil
	*/
	return nil
}
