// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"io"
	"testing"
	//"os"

	"code.cloudfoundry.org/bytefmt"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	//"github.com/sirupsen/logrus"
)

var (
	Manifest = "./transactional_test.yml"

	DLClient    *Client
	MockingData []byte
)

func init() {
	viper.SetConfigFile(Manifest)
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	man, err := manifest.New(viper.GetViper())
	if err != nil {
		panic(err)
	}

	// Stablestorage
	client, err := NewClient(Config{Manifest: man})
	if err != nil {
		panic(err)
	}
	DLClient = client

	// Dummy data
	filesize, err := bytefmt.ToBytes("2M")
	if err != nil {
		panic(err)
	}
	MockingData = make([]byte, filesize)

	// Set logger preferences
	//logrus.SetLevel(logrus.DebugLevel)
}

func TestBeginTx(t *testing.T) {
	var err error
	_, err = DLClient.BeginTx("", WRONLY)
	assert.Equal(t, err, ErrInvalidKey, "Empty string on WRONLY")

	_, err = DLClient.BeginTx("", RDONLY)
	assert.Equal(t, err, ErrInvalidKey, "Empty string on RDONLY")

	_, err = DLClient.BeginTx("", VIEW)
	assert.Equal(t, err, ErrInvalidKey, "Empty string on VIEW")

	_, err = DLClient.BeginTx("test", 4)
	assert.Equal(t, err, ErrInvalid, "random mode")
}

func TestTxWR(t *testing.T) {

	err := DLClient.CreateIfNotExist("")
	assert.Equal(t, err, ErrInvalidKey, "Empty key should not be allowed")

	key := uuid.Once()

	_, err = DLClient.BeginTx(key, WRONLY)
	assert.NotNil(t, err) // File does not exist

	err = DLClient.CreateIfNotExist(key)
	assert.Nil(t, err)

	tx, err := DLClient.BeginTx(key, WRONLY)
	assert.Nil(t, err)

	for i := 0; i < 2; i++ {
		err := tx.Update(0, func(pw io.ReadWriteCloser) (int, error) {
			wb, err := pw.Write(MockingData)
			return wb, err
		})
		assert.Nil(t, err)
	}
}
