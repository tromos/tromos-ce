// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/middleware/coordinatormanager"
	local "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"
	"gitlab.com/tromos/tromos-ce/engine/middleware/devicemanager"
	"gitlab.com/tromos/tromos-ce/engine/middleware/processormanager"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gopkg.in/go-playground/validator.v9"
)

// Config is the configuration for the middleware client
type Config struct {
	Manifest manifest.Manifest `validate:"required"`
}

// NewClient returns a middleware client
func NewClient(config Config) (*Client, error) {

	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Error("Cannot instantiate middleware")
		return nil, err
	}

	// Join the virtual infrastructure and initiate whatever Microservices and
	// connectors are meant to be used on the insitu instance
	instance, err := peer.New(peer.Config{Manifest: config.Manifest})
	if err != nil {
		return nil, err
	}

	// Configure Metadata mesh
	conf := config.Manifest.CoordinatorManagerConfiguration()
	plugin, err := instance.OpenSelectorPlugin(manifest.Plugin(conf))
	if err != nil {
		return nil, err
	}
	nm, err := coordinatormanager.New(coordinatormanager.Config{
		Peer:     instance,
		Selector: plugin(conf),
	})
	if err != nil {
		return nil, err
	}

	// Configure Data mesh
	conf = config.Manifest.DeviceManagerConfiguration()
	plugin, err = instance.OpenSelectorPlugin(manifest.Plugin(conf))
	if err != nil {
		return nil, err
	}
	dm, err := devicemanager.New(devicemanager.Config{
		Peer:     instance,
		Selector: plugin(conf),
	})
	if err != nil {
		return nil, err
	}

	// Configure Processing mesh
	conf = config.Manifest.ProcessorManagerConfiguration()
	plugin, err = instance.OpenSelectorPlugin(manifest.Plugin(conf))
	if err != nil {
		return nil, err
	}
	pm, err := processormanager.New(processormanager.Config{
		Peer:     instance,
		Selector: plugin(conf),
	})
	if err != nil {
		return nil, err
	}

	// Setup Datapath (Processing + Storage)
	dp, err := local.NewDatapath(local.Config{
		ProcessorManager: pm,
		DeviceManager:    dm,
	})
	if err != nil {
		return nil, err
	}

	logrus.Printf("Tromos successfully kickstarted manifest %s", config.Manifest.ID())
	return &Client{
		logger:             logrus.WithField("middleware", config.Manifest.ID()),
		config:             config,
		peer:               instance,
		DeviceManager:      dm,
		ProcessorManager:   pm,
		CoordinatorManager: nm,
		Datapath:           dp,
	}, nil
}

// Client provides access to the backend proxies
type Client struct {
	logger *logrus.Entry
	config Config
	peer   *peer.Peer

	ProcessorManager   *processormanager.ProcessorManager
	DeviceManager      *devicemanager.DeviceManager
	CoordinatorManager *coordinatormanager.CoordinatorManager
	Datapath           processor.Processor
}

// Close closes the client
func (client *Client) Close() error {
	if err := client.Datapath.Close(); err != nil {
		return err
	}

	if err := client.ProcessorManager.Close(); err != nil {
		return err
	}

	if err := client.DeviceManager.Close(); err != nil {
		return err
	}

	if err := client.CoordinatorManager.Close(); err != nil {
		return err
	}

	if err := client.peer.Close(); err != nil {
		return err
	}

	client.logger.Print("Successfully shutdown")
	return nil
}

// Remove deletes the log for the key from the respective Coordinator
func (client *Client) Remove(key string) error {
	if key == "" {
		return ErrInvalidKey
	}

	partition, err := client.CoordinatorManager.Partition(key)
	if err != nil {
		return err
	}
	return partition.SetLandmark(key, coordinator.Landmark{Disappear: true})
}

// Truncate resets the log the key from the respective Coordinator
func (client *Client) Truncate(key string) error {
	if key == "" {
		return ErrInvalidKey
	}

	partition, err := client.CoordinatorManager.Partition(key)
	if err != nil {
		return err
	}

	return partition.SetLandmark(key, coordinator.Landmark{IgnorePrevious: true})
}

// CreateOrReset ensures the log the key from the respective Coordinator
func (client *Client) CreateOrReset(key string) error {
	if key == "" {
		return ErrInvalidKey
	}

	partition, err := client.CoordinatorManager.Partition(key)
	if err != nil {
		return err
	}

	return partition.CreateOrReset(key)
}

// CreateIfNotExist creates the log the key from the respective Coordinator
func (client *Client) CreateIfNotExist(key string) error {
	if key == "" {
		return ErrInvalidKey
	}

	partition, err := client.CoordinatorManager.Partition(key)
	if err != nil {
		return err
	}

	return partition.CreateIfNotExist(key)
}
