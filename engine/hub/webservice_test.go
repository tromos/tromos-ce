// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package hub

import (
	"reflect"
	"testing"
)

func TestHub_getPlugin(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.hub.getPlugin(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hub.getPlugin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Hub.getPlugin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRunDistributionService(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			RunDistributionService()
		})
	}
}

func TestHub_RunDistributionService(t *testing.T) {
	tests := []struct {
		name string
		hub  *Hub
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.hub.RunDistributionService()
		})
	}
}

func TestHub_GetPluginFromDistributionService(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		hub     *Hub
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.hub.GetPluginFromDistributionService(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("Hub.GetPluginFromDistributionService() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
