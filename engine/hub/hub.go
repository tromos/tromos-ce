// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package hub // import "gitlab.com/tromos/tromos-ce/engine/hub"

import (
	"context"
	"crypto/sha256"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"plugin"
	"time"

	"github.com/gofrs/flock"
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	//"gitlab.com/tromos/tromos-ce/pkg/beautify"
	"gopkg.in/go-playground/validator.v9"
	//"github.com/hashicorp/go-getter"
)

// Config includes the execution parameters of the HUB
type Config struct {
	// Path is the location where hub will store the plugins. Validator
	// automatically ensures that the path is present. If it is not, it fails
	Path string `validate:"required,dir"`
}

// Hub is a insitu hub, or repository,  of modules and schemas
type Hub struct {
	ctx    context.Context
	config Config
	path   string
	logger *logrus.Entry
	locker *flock.Flock
}

var validate *validator.Validate = validator.New()

// NewHub creates a New hub on the $Root/plugin directory and keep an exlusive
// lock for as long as the process is running
func New(config Config) (*Hub, error) {

	if config == (Config{}) {
		logrus.WithError(ErrInvalid).Error("Cannot create new Hub")
		return nil, ErrInvalid
	}

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Error("Cannot create new Hub")
		return nil, ErrInvalid
	}

	// Obtain exclusive lock on the path. No other Hub instance should be able
	// to access it (This is to protect it against when multiple hubs during
	// testing)
	logrus.Debugf("Obtain lock for %s", config.Path)

	locker := flock.New(config.Path + ".lock")
	ok, err := locker.TryLockContext(context.Background(), time.Duration(20))
	if err != nil {
		return nil, err
	}

	if !ok {
		panic("This should level happen")
	}

	logrus.Printf("New Hub instantiated at %s", config.Path)

	return &Hub{
		ctx:    context.Background(),
		config: config,
		logger: logrus.WithField("Tromos", "Hub"),
		locker: locker,
	}, nil
}

// Close releases the lock on the Hub directory
func (hub *Hub) Close() error {
	return hub.locker.Unlock()
}

// shortenPath returns a hash of the path name. It is primarily create hashes of the
// given names and maintain the binary plugins in a flat namespace
func (hub *Hub) shortenPath(name string) string {
	pluginID := fmt.Sprintf("%x", sha256.Sum256([]byte(name)))

	return hub.config.Path + "/plugins/" + pluginID
}

// DownloadPlugins install the list of plugins.
func (hub *Hub) DownloadPlugins(list ...string) error {

	if len(list) == 0 {
		hub.logger.Warn("The given plugin list is empty")
		return nil
	}

	for _, name := range list {
		pluginpath := hub.shortenPath(name)

		if _, err := os.Stat(pluginpath); err == nil {
			// TODO: version checkking
			continue
		}

		// Names with .bin postfic are binary plugin loaded directly into the system.
		// Otherwise we must download and compile the plugin source code
		suffix := filepath.Ext(name)
		if suffix == "bin" {
			panic("Not implemented yet")
			/*
				if err := hub.GetPluginBinary(name); err != nil {
					return err
				}
			*/
		}
		if err := hub.GetPluginSource(name); err != nil {
			return err
		}

	}
	return nil
}

// GetPluginSource uses the go get command to download and compiles the plugin
func (hub *Hub) GetPluginSource(name string) error {

	if name == "" {
		return ErrInvalid
	}
	pluginpath := hub.shortenPath(name)

	hub.logger.WithFields(logrus.Fields{
		"Plugin": name,
		"Path":   pluginpath,
	}).Info("Compile Plugin")

	var err error
	//beautify.Progressbar(func() {
	/*
	 TODO: link the stdout and stderr to the respective logrus outputs.
	 Until then, omit the command's output
	*/
	cmd := exec.Command(
		"go", "build", "-race",
		"-o", pluginpath,
		"-buildmode=plugin", name,
	)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	//	})
	return err
}

/*
// GetPluginBinary downloads the binary plugin
func (hub *Hub) GetPluginBinary(name string) error {

	if name == "" {
		return ErrInvalid
	}
	pluginpath := hub.shortenPath(name)

	// release resources if get operation completes before time elapses
	ctx, cancel := context.WithTimeout(hub.ctx, configuration.Env.GetDuration("GetBinaryPluginTimeout"))
	defer cancel()

	client := &getter.Client{
		Ctx:     ctx,
		Src:     name,
		Dst:     pluginpath,
		Mode:    getter.ClientModeFile,
		Options: []getter.ClientOption{getter.WithProgress(&beautify.ProgressBar{})},
	}

	hub.logger.Infof("Fetch plugin %s [%s]", name, pluginpath)

	if err := client.Get(); err != nil {
		hub.logger.WithError(err).Warnf("Download error %s", hub.shortenPath(name))
		return err
	}
	return nil
}
*/

// OpenPlugin opens a plugin.
func (hub *Hub) OpenPlugin(name string) (interface{}, error) {

	if name == "" {
		hub.logger.WithError(ErrInvalid).Error("OpenPlugin")
		return nil, ErrInvalid
	}
	pluginpath := hub.shortenPath(name)

	hub.logger.WithFields(logrus.Fields{
		"Plugin": name,
		"Path":   pluginpath,
	}).Info("Open Plugin")

	table, err := plugin.Open(pluginpath)
	if err != nil {
		return nil, err
	}

	symbol, err := table.Lookup("Plugin")
	if err != nil {
		return nil, err
	}
	return symbol, nil
}

// OpenDevicePlugin opens a Device plugin
func (hub *Hub) OpenDevicePlugin(name string) (device.Plugin, error) {
	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}
	return *symbol.(*device.Plugin), nil
}

// OpenCoordinatorPlugin opens a Coordinator plugin
func (hub *Hub) OpenCoordinatorPlugin(name string) (coordinator.Plugin, error) {
	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}
	return *symbol.(*coordinator.Plugin), nil
}

// OpenCoordinatorPlugin opens a Coordinator plugin
func (hub *Hub) OpenProcessorPlugin(name string) (processor.Plugin, error) {
	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}

	return *symbol.(*processor.Plugin), nil
}

// OpenSelectorPlugin opens a Selector plugin
func (hub *Hub) OpenSelectorPlugin(name string) (selector.Plugin, error) {
	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}
	return *symbol.(*selector.Plugin), nil
}
