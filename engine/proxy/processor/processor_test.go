// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/tromos-ce/engine/proxy/processor"

import (
	"io"
	"io/ioutil"
	"strings"
	"sync"
	"testing"

	"code.cloudfoundry.org/bytefmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"go.uber.org/goleak"
)

var (
	MockingData []byte
	exampleLAFS string = `
--- 
Name: "Processor Default Configuration"
Description: "Default configuration with reasonable values"
Processors:
 lafs0:
  Capabilities: [ "default", "inmemory" ]
  MaxConcurrentChannels: 50
  Graph:
    plugin: gitlab.com/tromos/hub/processor/raid1
    integritycheck: true
    encrypt: true
    passphrase: lolabunnysixteen
    replicas: 3
`
)

func defaultConfig(complex bool) Config {

	var config *viper.Viper
	var name string
	if complex {
		viper.SetConfigType("yaml")
		name = "lafs0"
		if err := viper.ReadConfig(strings.NewReader(exampleLAFS)); err != nil {
			panic(err)
		}
		config = viper.GetViper()
	} else {
		name = "Default0"
		config = manifest.DefaultConfig(0, 1, 0)
	}

	man, err := manifest.New(config)
	if err != nil {
		panic(err)
	}

	hub, err := hub.New(hub.Config{Path: man.Hub()})
	if err != nil {
		panic(err)
	}

	if err := hub.DownloadPlugins(man.PluginList()...); err != nil {
		panic(err)
	}

	return Config{
		ID:         name,
		Parameters: man.ProcessorConfiguration(name),
		Hub:        hub,
	}
}

func init() {
	filesize, err := bytefmt.ToBytes("2M")
	if err != nil {
		panic(err)
	}
	MockingData = make([]byte, filesize)
}

// Test for goroutine leaking
func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestNew(t *testing.T) {
	var err error

	_, err = New(Config{})
	assert.NotNil(t, err) // Validator error

	proc, err := New(defaultConfig(false))
	assert.Nil(t, err)

	err = proc.Close()
	assert.Nil(t, err)

	err = proc.Close()
	assert.Equal(t, err, processor.ErrClosed, "Processor is already closed")
}

func TestNewChannel(t *testing.T) {

	proc, err := New(defaultConfig(false))
	assert.Nil(t, err)

	// Upstream
	{
		ch, err := proc.NewChannel(processor.ChannelConfig{Writable: true})
		assert.Nil(t, err)

		err = ch.Close()
		assert.Nil(t, err)

		err = ch.Close()
		assert.Equal(t, err, processor.ErrClosed, "Processor Channel is already closed")
	}

	// Downstream
	{
		ch, err := proc.NewChannel(processor.ChannelConfig{Writable: false})
		assert.Nil(t, err)

		err = ch.Close()
		assert.Nil(t, err)

		err = ch.Close()
		assert.Equal(t, err, processor.ErrClosed, "Processor Channel is already closed")
	}

	err = proc.Close()
	assert.Nil(t, err)
}

func TestNewTransferWrite(t *testing.T) {
	// Direct
	newTransferWrite(t, defaultConfig(false))

	// Multi
	newTransferWrite(t, defaultConfig(true))
}

func newTransferWrite(t *testing.T, config Config) {
	proc, err := New(config)
	assert.Nil(t, err)

	ch, err := proc.NewChannel(processor.ChannelConfig{Writable: true})
	assert.Nil(t, err)

	err = ch.NewTransfer(processor.Stream{})
	assert.Equal(t, err, processor.ErrInvalid, "Test empty struct")

	// Test valid stream, no data
	{
		pr, pw := processor.Pipe()
		err = ch.NewTransfer(processor.Stream{
			Data: pr,
			Port: "test",
			Meta: &processor.StreamMetadata{},
		})
		assert.Nil(t, err)

		err = pw.Close()
		assert.Nil(t, err)
	}

	// Test valid stream, with data
	for i := 0; i < 3; i++ {
		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pr,
			Port: "mockwriter",
			Meta: &processor.StreamMetadata{},
		}

		err = ch.NewTransfer(stream)
		assert.Nil(t, err)

		n, err := pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		n, err = pw.Write(MockingData)
		assert.Nil(t, err)
		assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")

		err = pw.Close()
		assert.Nil(t, err)
	}

	err = ch.Close()
	assert.Nil(t, err)

	err = proc.Close()
	assert.Nil(t, err)
}

func TestNewTransferRead(t *testing.T) {
	// Direct
	newTransferRead(t, defaultConfig(false))

	// Multi
	newTransferRead(t, defaultConfig(true))
}

func newTransferRead(t *testing.T, config Config) {

	proc, err := New(config)
	assert.Nil(t, err)

	ch, err := proc.NewChannel(processor.ChannelConfig{Writable: false})
	assert.Nil(t, err)

	// Test invalid stream input
	{
		err = ch.NewTransfer(processor.Stream{})
		assert.Equal(t, err, processor.ErrInvalid, "Test empty struct")

		// Conditional: Totalsize but not items
		_, pw := processor.Pipe()
		err = ch.NewTransfer(processor.Stream{
			Data: pw,
			Port: "test",
			Meta: &processor.StreamMetadata{TotalSize: len(MockingData)},
		})
		assert.Equal(t, err, processor.ErrInvalid, "Missing Items")
	}

	// Test valid stream, no data
	for i := 0; i < 3; i++ {
		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pw,
			Port: "test",
			Meta: &processor.StreamMetadata{TotalSize: 0},
		}

		err := ch.NewTransfer(stream)
		assert.Nil(t, err)

		n, err := io.Copy(ioutil.Discard, pr)
		if err != nil {
			logrus.Error("Err:", err)
		}
		assert.Nil(t, err)
		assert.Equal(t, n, int64(0), "0 Length read")
	}

	// Test valid stream, with data
	for i := 0; i < 30; i++ {
		pr, pw := processor.Pipe()

		stream := processor.Stream{
			Data: pw,
			Port: "test",
			Meta: &processor.StreamMetadata{
				TotalSize: len(MockingData),
				State:     make(map[string]string),
				Items: map[string]device.Item{
					"RandomItem": {
						ID:   "BackendIdentifier",
						Size: uint64(len(MockingData)),
					},
				},
			},
		}

		err := ch.NewTransfer(stream)
		assert.Nil(t, err)

		n, err := io.Copy(ioutil.Discard, pr)
		if err != nil {
			logrus.Error("Copy Error:", err)
		}
		assert.Nil(t, err)
		assert.Equal(t, n, int64(len(MockingData)), "Should read len(MockingData) bytes")

		n, err = io.Copy(ioutil.Discard, pr)
		if err != nil {
			logrus.Error("Copy Error:", err)
		}
		assert.Nil(t, err)
		assert.Equal(t, n, int64(0), "Should read 0 bytes since the pipe is closed")
	}
	err = ch.Close()
	assert.Nil(t, err)

	err = proc.Close()
	assert.Nil(t, err)
}

func TestInterleaving(t *testing.T) {
	// Direct
	interleaving(t, defaultConfig(false))

	// Multi
	interleaving(t, defaultConfig(true))
}

func interleaving(t *testing.T, config Config) {
	proc, err := New(config)
	assert.Nil(t, err)

	ch, err := proc.NewChannel(processor.ChannelConfig{Writable: true})
	assert.Nil(t, err)

	pr0, pw0 := processor.Pipe()
	stream0 := processor.Stream{
		Data: pr0,
		Port: "mockwriter0",
		Meta: &processor.StreamMetadata{},
	}
	err = ch.NewTransfer(stream0)
	assert.Nil(t, err)

	pr1, pw1 := processor.Pipe()
	stream1 := processor.Stream{
		Data: pr1,
		Port: "mockwriter1",
		Meta: &processor.StreamMetadata{},
	}
	err = ch.NewTransfer(stream1)
	assert.Nil(t, err)

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			n, err := pw0.Write(MockingData)
			assert.Nil(t, err)
			assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")
		}

		err := pw0.Close()
		assert.Nil(t, err)

		n, err := pw0.Write(MockingData)
		assert.Equal(t, err, io.ErrClosedPipe, "Pipe is closed")
		assert.Equal(t, n, 0, "Should write 0 bytes")

	}()

	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			n, err := pw1.Write(MockingData)
			assert.Nil(t, err)
			assert.Equal(t, n, len(MockingData), "Should write len(MockingData) bytes")
		}

		err := pw1.Close()
		assert.Nil(t, err)

		n, err := pw1.Write(MockingData)
		assert.Equal(t, err, io.ErrClosedPipe, "Pipe is closed")
		assert.Equal(t, n, 0, "Should write 0 bytes")
	}()
	wg.Wait()

	err = ch.Close()
	assert.Nil(t, err)

	err = proc.Close()
	assert.Nil(t, err)

}
