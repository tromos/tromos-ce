// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/tromos-ce/engine/proxy/processor"

import (
	"context"
	"sync"

	pool "github.com/jolestar/go-commons-pool"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gopkg.in/go-playground/validator.v9"
)

// Config defines the running paramers of the Processor
type Config struct {
	ID         string       `validate:"required"`
	Parameters *viper.Viper `validate:"required"`
	Hub        *hub.Hub     `validate:"required"`
}

// Processor is a virtual proxy composed out of a directed acyclic graph
type Processor struct {
	config                *Config
	maxConcurrentChannels int

	capabilities []selector.Capability
	upstreams    upstream
	downstreams  downstream

	peer     string
	logger   *logrus.Entry
	closed   bool
	reusable bool
}

var _ processor.Processor = (*Processor)(nil)
var validate *validator.Validate = validator.New()

// New returns a new Processor instance
func New(config Config) (*Processor, error) {

	if config == (Config{}) {
		return nil, processor.ErrInvalid
	}

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Print("Cannot create new Processor")
		return nil, processor.ErrInvalid
	}

	graphconfig := config.Parameters.Sub("graph")
	plugin, err := config.Hub.OpenProcessorPlugin(graphconfig.GetString("plugin"))
	if err != nil {
		return nil, err
	}

	graph := plugin(graphconfig)

	// Convert capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, capability := range config.Parameters.GetStringSlice("capabilities") {
		capability, ok := selector.Capabilities[capability]
		if !ok {
			logrus.WithField("Capability", capability).Error(processor.ErrCapability)
			return nil, processor.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	p := &Processor{
		peer:                  "127.0.0.1",
		config:                &config,
		capabilities:          capabilities,
		maxConcurrentChannels: config.Parameters.GetInt("maxconcurrentchannels"),
		logger:                logrus.WithField("Processor", config.ID),
	}
	p.init(graph)

	logrus.Infof("Found Processor %s @ %s", p.String(), p.Location())
	return p, nil
}

func (p *Processor) init(graph processor.ProcessGraph) {
	// function for compile at runtime
	p.upstreams.generate = func() *builder {
		return p.newbuilder(true, graph)
	}
	p.downstreams.generate = func() *builder {
		return p.newbuilder(false, graph)
	}

	// Enable for reusing the compiled graph
	// If not reusable, we cannot use pool
	//p.reusable = graph.Reusable()
	p.reusable = false
	if !p.reusable {
		return
	}

	// generic pool configuration
	uctx := context.Background()
	config := &pool.ObjectPoolConfig{
		LIFO:                 true,
		MaxTotal:             p.maxConcurrentChannels,
		MaxIdle:              -1,
		MinEvictableIdleTime: -1,
		BlockWhenExhausted:   false,
	}
	p.logger.Print("Poolsize:", config.MaxTotal)

	// Set up pools specifics
	p.upstreams.pool = pool.NewObjectPool(uctx,
		pool.NewPooledObjectFactorySimple(
			func(context.Context) (interface{}, error) {
				return p.upstreams.generate(), nil
			}),
		config,
	)
	pool.Prefill(uctx, p.upstreams.pool, p.maxConcurrentChannels)

	p.downstreams.pool = pool.NewObjectPool(uctx,
		pool.NewPooledObjectFactorySimple(
			func(context.Context) (interface{}, error) {
				return p.downstreams.generate(), nil
			}),
		config,
	)
	pool.Prefill(uctx, p.downstreams.pool, p.maxConcurrentChannels)

	// And make the generate to get items frm the pool
	p.upstreams.generate = func() *builder {
		obj, err := p.upstreams.pool.BorrowObject(uctx)
		if err != nil {
			panic(err)
		}
		return obj.(*builder)
	}

	p.downstreams.generate = func() *builder {
		obj, err := p.downstreams.pool.BorrowObject(uctx)
		if err != nil {
			panic(err)
		}
		return obj.(*builder)
	}
}

// String returns a descriptive name for the Processor
func (p *Processor) String() string {
	if p.closed {
		panic(processor.ErrClosed)
	}

	return p.config.ID
}

// Location returns the node where the Processor is running
func (p *Processor) Location() string {
	if p.closed {
		panic(processor.ErrClosed)
	}

	return p.peer
}

// Capabilities returns the capabilities of the Processor
func (p *Processor) Capabilities() []selector.Capability {
	if p.closed {
		panic(processor.ErrClosed)
	}

	return p.capabilities
}

// NewChannel returns a new channel associated to the Processor
func (p *Processor) NewChannel(config processor.ChannelConfig) (processor.Channel, error) {
	if p.closed {
		panic(processor.ErrClosed)
	}

	// If validation finds no error, eventually it can be replaced
	// by this filling method
	config.FillMissingFields()

	// Validate channel configuration
	if err := validate.Struct(config); err != nil {
		p.logger.Error("Err:", err)
		return nil, processor.ErrInvalid
	}

	var builder *builder
	if config.Writable {
		builder = p.upstreams.generate()
	} else {
		builder = p.downstreams.generate()
	}

	return &channel{
		proc:    p,
		config:  config,
		logger:  logrus.WithField("Channel", config.Name),
		builder: builder,
	}, nil
}

// Close gracefully shutdowns the processor
func (p *Processor) Close() error {
	if p.closed {
		return processor.ErrClosed
	}
	p.closed = true

	// Close the pools if they exist. They may not exist though if
	// the graph is not reusable
	if p.upstreams.pool != nil {
		p.logger.Info("Close upsterams")
		p.upstreams.pool.Close(context.Background())
	}
	if p.downstreams.pool != nil {
		p.logger.Info("Close downstreams")
		p.downstreams.pool.Close(context.Background())
	}
	p.logger.Info("Processor has been successfully closed")
	return nil
}

type channel struct {
	locker sync.Mutex
	proc   *Processor

	config processor.ChannelConfig
	logger *logrus.Entry

	builder *builder

	closed bool
}

func (ch *channel) Config() processor.ChannelConfig {
	if ch.closed {
		panic(processor.ErrClosed)
	}
	return ch.config
}

func (ch *channel) Capabilities(port string) []selector.Capability {
	return ch.builder.capabilitymap[port]
}

func (ch *channel) NewTransfer(stream processor.Stream) error {
	// Prevent NewTransfer() while closing
	ch.locker.Lock()
	defer ch.locker.Unlock()

	if ch.closed {
		panic(processor.ErrClosed)
	}

	// Validate stream input
	if err := validate.Struct(stream); err != nil {
		ch.logger.Error("Err:", err)
		return processor.ErrInvalid
	}

	// Push data for process in the DAG
	ch.builder.inport <- stream

	go func() {
		for stream := range ch.builder.serve() {
			ch.config.Pushout(stream)
		}
	}()
	return nil
}

func (ch *channel) Sinks() map[string]string {
	if ch.closed {
		panic(processor.ErrClosed)
	}

	sinks := make(map[string]string)
	for _, port := range ch.builder.outports {
		sinks[port] = ""
	}
	return sinks
}

func (ch *channel) Close() error {
	ch.logger.Print("Channel Close()")
	// Prevent NewTransfer() while closing
	ch.locker.Lock()
	defer ch.locker.Unlock()

	if ch.closed {
		return processor.ErrClosed
	}
	ch.closed = true

	if ch.proc.reusable {
		return ch.builder.reset()
	}
	return ch.builder.Close()
}
