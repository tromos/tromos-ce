// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/tromos-ce/engine/proxy/processor"

import (
	"context"
	"reflect"

	pool "github.com/jolestar/go-commons-pool"
	"github.com/sirupsen/logrus"
	flow "github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
)

type upstream struct {
	pool     *pool.ObjectPool
	generate func() *builder
}

type downstream struct {
	pool     *pool.ObjectPool
	generate func() *builder
}

type builder struct {
	processor *Processor
	logger    *logrus.Entry

	// Internal network
	network       *flow.Graph
	writable      bool
	capabilitymap map[string][]selector.Capability // port -> key -> value

	// External network
	quit chan struct{}

	inport     chan processor.Stream // Input to the process network
	outports   []string              // Port identifiers
	readyports []reflect.SelectCase  // Ports ready to send/receive data
}

func (p *Processor) newbuilder(writable bool, graph processor.ProcessGraph) *builder {

	builder := &builder{
		processor:     p,
		writable:      writable,
		capabilitymap: make(map[string][]selector.Capability),

		logger: logrus.WithField("Builder", "builder"),
	}

	// Graph constructor and structure definition of IO network
	// creates the object in heap
	network := new(flow.Graph)
	network.InitGraphState()
	builder.network = network

	// populate the blank network with processes
	if writable {
		graph.Upstream(builder)
	} else {
		graph.Downstream(builder)
	}

	// Handle inputs and outputs of the network
	inport := make(chan processor.Stream)
	network.SetInPort("In", inport)

	outportsV := reflect.ValueOf(*network).FieldByName("outPorts").MapKeys()
	outports := make([]string, len(outportsV))
	for i := 0; i < len(outportsV); i++ {
		outports[i] = outportsV[i].String()
	}

	// The first notification designates termination of the high-level operation.
	// All the other designate data on the sinks that participate on
	// the high-level operation
	readyports := make([]reflect.SelectCase, len(outports)+1)
	quit := make(chan struct{})

	readyports[0] = reflect.SelectCase{
		Dir:  reflect.SelectRecv,
		Chan: reflect.ValueOf(quit),
	}

	for i := 1; i < len(readyports); i++ {
		ch := make(chan processor.Stream)
		network.SetOutPort(outports[i-1], ch)
		readyports[i] = reflect.SelectCase{
			Dir:  reflect.SelectRecv,
			Chan: reflect.ValueOf(ch),
		}
	}

	// Wait until the net has completed its job
	flow.RunNet(network)
	<-network.Ready()

	// Set values for builder runtime
	builder.inport = inport
	builder.outports = outports
	builder.readyports = readyports
	builder.quit = quit

	return builder
}

func (b *builder) serve() <-chan processor.Stream {
	events := make(chan processor.Stream)

	go func(events chan processor.Stream) {
		for {
			chosen, value, _ := reflect.Select(b.readyports)
			// Port 0 indicates termination
			if chosen == 0 {
				// Acknowledge termination
				close(events)
				return
			}

			substream := value.Interface().(processor.Stream)
			// This may happen for modules that on donwlink do not wish to consume
			// data from all the connected sinks (e.g., mirror)
			if (processor.Stream{}) == substream {
				continue
			}
			substream.Port = b.outports[chosen-1]
			events <- substream
		}
	}(events)
	return events
}

func (b *builder) reset() error {
	// If applicable, return the builder object to the pool
	if b.writable && b.processor.upstreams.pool != nil {
		b.quit <- struct{}{}
		return b.processor.upstreams.pool.ReturnObject(context.Background(), b)
	}
	if !b.writable && b.processor.downstreams.pool != nil {
		b.quit <- struct{}{}
		return b.processor.downstreams.pool.ReturnObject(context.Background(), b)
	}

	return nil
}

func (b *builder) Close() error {
	b.logger.Print("Close inport")
	// Close the input to shutdown the network down
	close(b.inport)

	b.logger.Print("Wait for network")
	// Wait until the network has done its job
	<-b.network.Wait()

	b.logger.Print("Stop network")
	// Tell the runtime to shut down
	b.network.Stop()

	b.logger.Print("Propage cancellation")
	// Propagate cancellation to all Processor outputs
	close(b.quit)

	return nil
}

// Add adds a new process with a given name to the DAG
func (b *builder) Add(name string, module interface{}) bool {
	return b.network.Add(module, name)
}

// MapInPort adds an inport to the DAG and maps it to a contained module's port.
func (b *builder) MapInPort(port, app, appPort string) {
	if !b.network.MapInPort(port, app, appPort) {
		panic("MapInPort failed")
	}
}

// Connect connects a sender to a receiver module
func (b *builder) Connect(senderName, senderPort, receiverName, receiverPort string) bool {
	return b.network.Connect(senderName, senderPort, receiverName, receiverPort)
}

// MapOutPort adds an outport to the DAG  and maps it to a contained module's port.
// Capability contain linking information with the next element (Processor or Device)
func (b *builder) MapOutPort(port, app, appPort string, annotations ...selector.Capability) {
	b.network.MapOutPort(port, app, appPort)
	b.capabilitymap[port] = annotations
}
