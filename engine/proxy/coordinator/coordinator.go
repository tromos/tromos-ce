// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package coordinator // import "gitlab.com/tromos/tromos-ce/engine/proxy/coordinator"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"gopkg.in/go-playground/validator.v9"
)

// Config defines the running paramers of the Coordinator
type Config struct {
	ID         string       `validate:"required"`
	Parameters *viper.Viper `validate:"required"`
	Hub        *hub.Hub     `validate:"required"`
}

var _ coordinator.Coordinator = (*Coordinator)(nil)
var validate *validator.Validate = validator.New()

// Coordinator is a virtual proxy composed out of several coordinator.Coordinator processing layers
type Coordinator struct {
	toplayer     coordinator.Coordinator
	config       *Config
	capabilities []selector.Capability

	peer string

	logger *logrus.Entry
	closed bool
}

// New returns a logical proxy Coordinator. It is used to store metadata
// to third-party databases
func New(config Config) (*Coordinator, error) {

	if config == (Config{}) {
		return nil, coordinator.ErrInvalid
	}

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Print("Cannot create new Coordinator")
		return nil, coordinator.ErrInvalid
	}

	var islocal bool
	peer := config.Parameters.GetString("Proxy.host")
	if peer == "" {
		peer = "127.0.0.1"
		islocal = true
	} else {
		islocal = net.IsLocalAddress(peer)
	}

	// It is insitu when 1) there is no proxy 2) the proxy ip is a insitu ip
	var layer coordinator.Coordinator
	if !islocal {
		peer = config.Parameters.GetString("Proxy.host")

		proxyclient := config.Parameters.Sub("proxy")
		plugin, err := config.Hub.OpenCoordinatorPlugin(
			proxyclient.GetString("plugin") + "/client")
		if err != nil {
			return nil, err
		}
		layer, err = plugin(proxyclient)
		if err != nil {
			return nil, err
		}
		layer.SetBackend(nil)
	} else {
		persistent := config.Parameters.Sub("persistent")

		plugin, err := config.Hub.OpenCoordinatorPlugin(persistent.GetString("plugin"))
		if err != nil {
			return nil, err
		}

		layer, err = plugin(persistent)
		if err != nil {
			return nil, err
		}
		layer.SetBackend(nil)

		// Load intermediate connectors (and change high-order accoringdly)
		stack := structures.SortMapStringKeys(config.Parameters.GetStringMap("Translators"))
		for _, seqID := range stack {
			translator := config.Parameters.Sub("translators." + seqID)

			plugin, err := config.Hub.OpenCoordinatorPlugin(translator.GetString("plugin"))
			if err != nil {
				return nil, err
			}

			newlayer, err := plugin(translator)
			if err != nil {
				return nil, err
			}

			newlayer.SetBackend(layer)
			layer = newlayer
		}

		// Local coordinator exposed through proxy (the proxy ip is the insitu ip)
		if len(config.Parameters.GetStringMapString("Proxy")) > 0 {
			proxyserver := config.Parameters.Sub("proxy")
			plugin, err := config.Hub.OpenCoordinatorPlugin(proxyserver.GetString("plugin") + "/server")
			if err != nil {
				return nil, err
			}

			server, err := plugin(proxyserver)
			if err != nil {
				return nil, err
			}

			server.SetBackend(layer)
		}
	}

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, capability := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[capability]
		if !ok {
			return nil, coordinator.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	c := &Coordinator{
		toplayer:     layer,
		config:       &config,
		capabilities: capabilities,
		peer:         peer,
		logger:       logrus.WithField("Coordinator", config.ID),
	}

	c.logger.Infof("Found Coordinator %s @ % s", c.String(), c.Location())
	return c, nil
}

// SetBackend defines the backend for the current device layer
func (c *Coordinator) SetBackend(_ coordinator.Coordinator) {
	if c.closed {
		panic(coordinator.ErrClosed)
	}
}

// String returns a descriptor for the current device layer
func (c *Coordinator) String() string {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.config.ID
}

// Capabilities returns the capabilities of the Coordinator
func (c *Coordinator) Capabilities() []selector.Capability {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.capabilities
}

// Location returns the node where the Coordinator is running
func (c *Coordinator) Location() string {
	if c.closed {
		panic(coordinator.ErrClosed)
	}
	return c.peer
}

// Close terminates the coordinator
func (c *Coordinator) Close() error {
	if c.closed {
		return coordinator.ErrClosed
	}
	c.closed = true

	return c.toplayer.Close()
}

// Info returns information for the key
func (c *Coordinator) Info(key string) ([][]byte, coordinator.Info, error) {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.Info(key)
}

// CreateOrReset creates a new logical file if it does not exist.
// If it exists, it resets all of the contents
func (c *Coordinator) CreateOrReset(key string) error {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.Close()
}

// CreateIfNotExist creates a new logical file if it does not exists.
// If it exists, it returns an error
func (c *Coordinator) CreateIfNotExist(key string) error {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.CreateIfNotExist(key)
}

// SetLandmark appends a landmark to the update log.
func (c *Coordinator) SetLandmark(key string, mark coordinator.Landmark) error {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.SetLandmark(key, mark)
}

// UpdateStart initiates a new transaction for writing to the logical file.
// It appends the intentions to the transaction log of the logical file
// identified by key
func (c *Coordinator) UpdateStart(key string, tid string, intentions coordinator.IntentionRecord) error {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.UpdateStart(key, tid, intentions)
}

// UpdateEnd appends a new entry on the update log to store
// metadata of a client transaction (tid). Once the write is complete, it atomically
// removes the respective entry from the transaction log
func (c *Coordinator) UpdateEnd(key string, tid string, record []byte) error {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.UpdateEnd(key, tid, record)
}

// ViewStart returns all the records (see landmark) for a logical file
// and keeps a lease to protect them from being garbage collected
func (c *Coordinator) ViewStart(key string, filter []string) (records [][]byte, tids []string, err error) {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.ViewStart(key, filter)
}

// ViewEnd removes the leases allocated during ViewStart
func (c *Coordinator) ViewEnd(tids []string) error {
	if c.closed {
		panic(coordinator.ErrClosed)
	}

	return c.toplayer.Close()
}
