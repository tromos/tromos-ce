// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package coordinator // import "gitlab.com/tromos/tromos-ce/engine/proxy/coordinator"

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"go.uber.org/goleak"
	//"github.com/sirupsen/logrus"
)

func defaultConfig() Config {

	man, err := manifest.New(manifest.DefaultConfig(0, 0, 1))
	if err != nil {
		panic(err)
	}

	hub, err := hub.New(hub.Config{Path: man.Hub()})
	if err != nil {
		panic(err)
	}

	if err := hub.DownloadPlugins(man.PluginList()...); err != nil {
		panic(err)
	}

	return Config{
		ID:         "Default0",
		Parameters: man.CoordinatorConfiguration("Default0"),
		Hub:        hub,
	}
}

// Test for goroutine leaking
func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}

func TestNew(t *testing.T) {
	var err error

	_, err = New(Config{})
	assert.NotNil(t, err) // Validator error

	_, err = New(Config{
		ID:         "",
		Parameters: nil,
	})
	assert.NotNil(t, err) // Validator error

	coord, err := New(defaultConfig())
	assert.Nil(t, err)

	err = coord.Close()
	assert.Nil(t, err)

	err = coord.Close()
	assert.Equal(t, err, coordinator.ErrClosed, "Coordinator is already closed")

}
