// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package manifest // import "gitlab.com/tromos/tromos-ce/engine/manifest"

import (
	"fmt"
	"io"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	_ "gitlab.com/tromos/tromos-ce/configuration" // Load default values
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
)

// generateDefaultProcessors create num default Processors
func generateDefaultProcessors(t *template.Template, manifest io.Writer, num int) {
	type processor struct {
		Name                  string
		Capabilities          string
		Plugin                string
		MaxConcurrentChannels int
	}

	processors := make([]processor, num)
	for i := 0; i < num; i++ {
		processors[i] = processor{
			Name:                  fmt.Sprintf("Default%d", i),
			Capabilities:          "[default, inmemory]",
			Plugin:                "gitlab.com/tromos/hub/processor/direct",
			MaxConcurrentChannels: 48,
		}
	}

	t, err := t.Parse(`
Processors:
{{range $k, $v := .}}
 {{.Name}}:
   Capabilities: {{.Capabilities}}
   MaxConcurrentChannels: {{.MaxConcurrentChannels}}
   Graph:
    plugin: {{.Plugin}}
{{end}}`)
	if err != nil {
		panic(err)
	}

	if err := t.Execute(manifest, processors); err != nil {
		panic(err)
	}
}

// generateDefaultCoordinators create num default coordinators
func generateDefaultCoordinators(t *template.Template, manifest io.Writer, num int) {
	type coordinator struct {
		Name   string
		Plugin string
	}

	coordinators := make([]coordinator, num)
	for i := 0; i < num; i++ {
		coordinators[i] = coordinator{
			Name:   fmt.Sprintf("Default%d", i),
			Plugin: "gitlab.com/tromos/hub/coordinator/boltdb",
		}
	}

	t, err := t.Parse(`
Coordinators:
{{range $k, $v := .}}
 {{.Name}}:
  Persistent:
   Plugin: {{.Plugin}}
   Path: /tmp/tromos/metadata/{{.Name}}
   Cleanstart: true
{{end}}`)
	if err != nil {
		panic(err)
	}

	if err := t.Execute(manifest, coordinators); err != nil {
		panic(err)
	}
}

// generateDefaultDevices create num default coordinators
func generateDefaultDevices(t *template.Template, manifest io.Writer, num int) {
	type device struct {
		Name         string
		Capabilities string
		Plugin       string
	}

	devices := make([]device, num)
	for i := 0; i < num; i++ {
		devices[i] = device{
			Name:         fmt.Sprintf("Default%d", i),
			Capabilities: "[default]",
			Plugin:       "gitlab.com/tromos/hub/device/filesystem",
		}
	}

	t, err := t.Parse(`
Devices:
{{range $k, $v := .}}
 {{.Name}}:
  Persistent:
   Plugin: {{.Plugin}}
   Path: inmem
   Family: memory
{{end}}`)
	if err != nil {
		panic(err)
	}

	if err := t.Execute(manifest, devices); err != nil {
		panic(err)
	}
}

// DefaultConfig returns a default configuration for the Manifest.
// The arguments indicate the number of devices, coordinators, and processors
// to be created
func DefaultConfig(numdevs, numprocs, numcoords int) *viper.Viper {
	t := template.New("manifest")
	manifest := &strings.Builder{}

	if _, err := manifest.WriteString(`
---
Name: "Laptop"
Description: "Default configuration with reasonable values"
Middleware:
 Format:
  plugin: "record"
 DeviceManager:
  plugin: gitlab.com/tromos/hub/selector/byqos
 CoordinatorManager:
  plugin: gitlab.com/tromos/hub/selector/consistenthash
 ProcessorManager:
  plugin: gitlab.com/tromos/hub/selector/random
`); err != nil {
		panic(err)
	}

	generateDefaultDevices(t, manifest, numdevs)
	generateDefaultProcessors(t, manifest, numprocs)
	generateDefaultCoordinators(t, manifest, numcoords)

	// Load the Manifest
	viper.SetConfigType("yaml")
	if err := viper.ReadConfig(strings.NewReader(manifest.String())); err != nil {
		panic(err)
	}

	return viper.GetViper()
}

// Manifest provides access to the elements described in the configuration file
type Manifest struct {
	config *viper.Viper
	logger *logrus.Entry
}

// New returns a new Manifest
func New(config *viper.Viper) (Manifest, error) {
	if config == nil {
		return Manifest{}, ErrInvalid
	}

	return Manifest{
		config: config,
		logger: logrus.WithField("Manifest", config.GetString("name")),
	}, nil
}

// PluginList returns the dependencies for connecting to the proxies
// (Device, Coordinators, Processors). If the peer is a local ip or nil value
// it will also return the stack for building the proxies. It will also return
// the dependencies for the DeviceManager, CoordinatorManager, and ProcessorManager
func (m Manifest) PluginList() []string {
	var pluginlist []string

	dm := m.DeviceManagerConfiguration()
	if dm != nil {
		pluginlist = append(pluginlist, dm.GetString("plugin"))
	}

	pm := m.ProcessorManagerConfiguration()
	if pm != nil {
		pluginlist = append(pluginlist, pm.GetString("plugin"))
	}

	cd := m.CoordinatorManagerConfiguration()
	if cd != nil {
		pluginlist = append(pluginlist, cd.GetString("plugin"))
	}

	for _, devname := range m.Devices() {
		dev := m.DeviceStack(devname)
		if dev != nil {
			pluginlist = append(pluginlist, dev...)
		}
	}

	for _, name := range m.Coordinators() {
		coord := m.CoordinatorStack(name)
		if coord != nil {
			pluginlist = append(pluginlist, coord...)
		}
	}

	for _, name := range m.Processors() {
		proc := m.ProcessorStack(name)
		if proc != nil {
			pluginlist = append(pluginlist, proc...)
		}
	}

	return structures.UniqueStrings(pluginlist)
}

// Root returns the root of executing Tromos instance
func (m Manifest) Root() string {
	return m.config.GetString("root")
}

// Hub returns the path where hub store the plugins
func (m Manifest) Hub() string {
	return m.config.GetString("hub")
}

// ID returns the identifier of the manifest
func (m Manifest) ID() string {
	return m.config.GetString("name")
}

// Description returns the description of the  manifest
func (m Manifest) Description() string {
	return m.config.GetString("description")
}

// Devices return all the Devices in the Manifest
func (m Manifest) Devices() []string {
	devmap := m.config.GetStringMap("devices")

	devices := make([]string, 0, len(devmap))
	for dev := range devmap {
		devices = append(devices, dev)
	}

	return devices
}

// DeviceStack returns the Device stack as presented on caller node. For example,
// if the Device is local it will return the whole stack. Otherwise only the
// proxy client for accessing the remote Device.
func (m Manifest) DeviceStack(devid string) []string {

	if devid == "" {
		panic("Invalid device identifier")
	}

	conf := m.DeviceConfiguration(devid)

	peer := conf.GetString("proxy.host")
	if peer == "" {
		peer = "127.0.0.1"
	}

	var stack []string
	if net.IsLocalAddress(peer) {
		stack = append(stack, conf.GetString("persistent.plugin"))

		translators := conf.GetStringMap("translators")
		for _, seqID := range structures.SortMapStringKeys(translators) {
			stack = append(stack, conf.GetString("translators."+seqID+".plugin"))
		}

		if conf.IsSet("proxy") {
			stack = append(stack,
				conf.GetString("proxy.plugin")+"/server",
				conf.GetString("proxy.plugin")+"/client",
			)
		}
	} else if conf.IsSet("proxy") {
		// The node is a client to a remote device
		return []string{conf.GetString("proxy.plugin") + "/client"}
	}

	return stack
}

// DeviceConfiguration returns the configuration for the Device
func (m Manifest) DeviceConfiguration(devid string) *viper.Viper {
	if devid == "" {
		panic(ErrInvalid)
	}

	return m.config.Sub("devices." + devid)
}

// Coordinators returns all the Coordinators in the Manifest
func (m Manifest) Coordinators() []string {
	coordmap := m.config.GetStringMap("coordinators")

	coordinators := make([]string, 0, len(coordmap))
	for coord := range coordmap {
		coordinators = append(coordinators, coord)
	}
	return coordinators
}

// CoordinatorStack returns the Coordinator stack as presented on caller node.
// For example,if the Coordinator is local it will return the whole stack.
// Otherwise only the proxy client for accessing the remote coordinator.
func (m Manifest) CoordinatorStack(cid string) []string {
	if cid == "" {
		panic("Invalid coordinator identifier")
	}

	conf := m.CoordinatorConfiguration(cid)

	peer := conf.GetString("proxy.host")
	if peer == "" {
		peer = "127.0.0.1"
	}

	var stack []string
	if net.IsLocalAddress(peer) {
		stack = append(stack, conf.GetString("persistent.plugin"))

		translators := conf.GetStringMap("translators")
		for _, seqID := range structures.SortMapStringKeys(translators) {
			stack = append(stack, conf.GetString("translators."+seqID+".plugin"))
		}

		if conf.IsSet("proxy") {
			stack = append(stack,
				conf.GetString("proxy.plugin")+"/server",
				conf.GetString("proxy.plugin")+"/client",
			)

		}
	} else if conf.IsSet("proxy") {
		// The node is a client to a remote device
		return []string{conf.GetString("proxy.plugin") + "/client"}
	}

	return stack
}

// CoordinatorConfiguration returns the configuration for the Coordinator
func (m Manifest) CoordinatorConfiguration(cid string) *viper.Viper {
	if cid == "" {
		panic(ErrInvalid)
	}

	return m.config.Sub("coordinators." + cid)
}

// Processors returns all the processors in the Manifest.
func (m Manifest) Processors() []string {
	procmap := m.config.GetStringMap("processors")

	processors := make([]string, 0, len(procmap))
	for proc := range procmap {
		processors = append(processors, proc)
	}
	return processors
}

// ProcessorStack returns the Processor stack as presented on caller node. For example,
// if the Processor is local it will return the whole stack. Otherwise only the
// proxy client for accessing the remote Processor.
func (m Manifest) ProcessorStack(pid string) []string {
	if pid == "" {
		panic(ErrInvalid)
	}

	conf := m.ProcessorConfiguration(pid)
	if conf != nil {
		return []string{conf.GetString("graph.plugin")}
	}
	return nil
}

// ProcessorConfiguration returns the configuration for the Processor.
func (m Manifest) ProcessorConfiguration(pid string) *viper.Viper {
	if pid == "" {
		panic(ErrInvalid)
	}
	return m.config.Sub("processors." + pid)
}

// CoordinatorManagerConfiguration returns the configuration for the CoordinatorManager
func (m Manifest) CoordinatorManagerConfiguration() *viper.Viper {
	return m.config.Sub("middleware.coordinatormanager")
}

// DeviceManagerConfiguration returns the configuration for the DeviceManager
func (m Manifest) DeviceManagerConfiguration() *viper.Viper {
	return m.config.Sub("middleware.devicemanager")
}

// ProcessorManagerConfiguration returns the configuration for the ProcessorManager
func (m Manifest) ProcessorManagerConfiguration() *viper.Viper {
	return m.config.Sub("middleware.processormanager")
}

func Plugin(conf *viper.Viper) string {
	plugin := conf.GetString("plugin")
	if plugin == "" {
		panic("Invalid configuration")
	}
	return plugin
}
