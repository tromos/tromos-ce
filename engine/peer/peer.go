// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package peer // import "gitlab.com/tromos/tromos-ce/engine/peer"

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	vcoord "gitlab.com/tromos/tromos-ce/engine/proxy/coordinator"
	vdev "gitlab.com/tromos/tromos-ce/engine/proxy/device"
	vproc "gitlab.com/tromos/tromos-ce/engine/proxy/processor"
	"gopkg.in/go-playground/validator.v9"
)

const (
	WebServicePort string = "6666"
)

// Config is the configuration for the Peer. If address is empty then the localhost
// is used instead
type Config struct {
	Manifest manifest.Manifest `validate:"required"`
	Address  string            `validate:"omitempty,ip"`
}

// Peer is a continuous configuration process that handles the proxies
type Peer struct {
	*hub.Hub

	config Config
	closed bool
	logger *logrus.Entry

	// Meshes of Instantiated Microservices
	devices      map[string]device.Device
	coordinators map[string]coordinator.Coordinator
	processors   map[string]processor.Processor

	webservice *http.Server
}

var validate *validator.Validate = validator.New()

// New sets up a new peer. Manifest is required. Address is optional.
func New(config Config) (peer *Peer, err error) {

	if config == (Config{}) {
		return nil, ErrInvalid
	}

	if err := validate.Struct(config); err != nil {
		logrus.WithError(err).Errorf("Cannot create new Peer")
		return nil, ErrInvalid
	}

	if config.Address == "" {
		config.Address = "127.0.0.1"
	}

	// Download the plugins for the runtime
	hub, err := hub.New(hub.Config{Path: config.Manifest.Hub()})
	if err != nil {
		return nil, err
	}

	if err := hub.DownloadPlugins(config.Manifest.PluginList()...); err != nil {
		return nil, err
	}

	// Initialize proxies (TODO: make it atomic)
	devices := make(map[string]device.Device)
	for _, id := range config.Manifest.Devices() {
		config := vdev.Config{
			ID:         id,
			Parameters: config.Manifest.DeviceConfiguration(id),
			Hub:        hub,
		}

		dev, err := vdev.New(config)
		if err != nil {
			return nil, err
		}
		devices[id] = dev
	}

	coordinators := make(map[string]coordinator.Coordinator)
	for _, id := range config.Manifest.Coordinators() {
		config := vcoord.Config{
			ID:         id,
			Parameters: config.Manifest.CoordinatorConfiguration(id),
			Hub:        hub,
		}

		coord, err := vcoord.New(config)
		if err != nil {
			return nil, err
		}
		coordinators[id] = coord
	}

	processors := make(map[string]processor.Processor)
	for _, id := range config.Manifest.Processors() {
		config := vproc.Config{
			ID:         id,
			Parameters: config.Manifest.ProcessorConfiguration(id),
			Hub:        hub,
		}

		proc, err := vproc.New(config)
		if err != nil {
			return nil, err
		}
		processors[id] = proc
	}

	return &Peer{
		Hub:          hub,
		config:       config,
		logger:       logrus.WithField("peer", config.Manifest.ID()),
		devices:      devices,
		coordinators: coordinators,
		processors:   processors,
	}, nil
}

// Address returns the ip address which the peer will use to spawn microservices. The
// address must be the same as in the Manifest
func (peer *Peer) Address() string {
	return peer.config.Address
}

// Close terminates the peer and all the bootstrapped Proxies
func (peer *Peer) Close() error {
	if peer.closed {
		return ErrClosed
	}
	peer.closed = true

	// TODO make it atomic
	for _, dev := range peer.devices {
		if err := dev.Close(); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	for _, coord := range peer.coordinators {
		if err := coord.Close(); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	for _, proc := range peer.processors {
		if err := proc.Close(); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	if peer.webservice != nil {
		if err := peer.webservice.Shutdown(context.TODO()); err != nil {
			peer.logger.WithError(err).Errorf("Peer shutdown error")
			return err
		}
	}

	return nil
}

// Devices return the names of the instantiated proxy Devices
func (peer *Peer) Devices() []string {
	if peer.closed {
		panic(ErrClosed)
	}

	names := make([]string, 0, len(peer.devices))
	for name := range peer.devices {
		names = append(names, name)
	}

	return names
}

// Device returns the Device corresponding to the name
func (peer *Peer) Device(name string) (device.Device, bool) {
	if peer.closed {
		panic(ErrClosed)
	}
	p, ok := peer.devices[name]
	return p, ok
}

// Coordinators return the names of the instantiated proxy Coordinators
func (peer *Peer) Coordinators() []string {
	if peer.closed {
		panic(ErrClosed)
	}

	names := make([]string, 0, len(peer.coordinators))
	for name := range peer.coordinators {
		names = append(names, name)
	}

	return names
}

// Coordinator returns the Coordinator corresponding to the name
func (peer *Peer) Coordinator(name string) (coordinator.Coordinator, bool) {
	if peer.closed {
		panic(ErrClosed)
	}
	p, ok := peer.coordinators[name]
	return p, ok
}

// Processors return the names of the instantiated proxy Processors
func (peer *Peer) Processors() []string {
	if peer.closed {
		panic(ErrClosed)
	}

	names := make([]string, 0, len(peer.processors))
	for name := range peer.processors {
		names = append(names, name)
	}

	return names
}

// Device returns the Processor corresponding to the name
func (peer *Peer) Processor(name string) (processor.Processor, bool) {
	if peer.closed {
		panic(ErrClosed)
	}
	p, ok := peer.processors[name]
	return p, ok
}

/*
*	nm.config.Selector.Walk(func(k []byte, _ interface{}) bool {
		if err := nm.config.Coordinators[string(k)].Close(); err != nil {
			return true
		}
		return false
	})
*/
