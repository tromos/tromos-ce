// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package main // import "gitlab.com/tromos/tromos-ce/cmd/tromos-cli"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/tromos/tromos-ce/cmd/tromos-cli/gateway"
	"gitlab.com/tromos/tromos-ce/cmd/tromos-cli/hub"
	"gitlab.com/tromos/tromos-ce/cmd/tromos-cli/manifest"
	"os"
	"os/signal"
)

func init() {
	// initial log formatting; this setting is updated after the daemon configuration is loaded.
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	RootCmd.AddCommand(gateway.Cmd)
	RootCmd.AddCommand(manifest.Cmd)
	RootCmd.AddCommand(hub.Cmd)
}

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:                   "tromos",
	Short:                 "Tromos command line tool",
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func main() {
	errChan := make(chan error, 1)
	go func() {
		defer close(errChan)
		if err := RootCmd.Execute(); err != nil {
			errChan <- err
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block waiting either an error to occur to a termination signal to come
	select {
	case <-c:
		signal.Reset(os.Interrupt)
	case err := <-errChan:
		if err != nil {
			logrus.Fatal(err)
		}
	}
}
