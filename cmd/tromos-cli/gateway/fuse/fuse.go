// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package fuse // import "gitlab.com/tromos/tromos-ce/cmd/tromos-cli/gateway/fuse"

import (
	"bazil.org/fuse"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	tromosfs "gitlab.com/tromos/tromos-ce/gateway/fuse"
)

func Check(err error) {
	if err != nil {
		panic(err)
	}
}

func init() {
	flags := Cmd.Flags()
	flags.String("mountpoint", "", "Where fuse will be mounted")
	flags.String("manifest", "", "Location to the manifest")

	Check(Cmd.MarkFlagRequired("mountpoint"))
	Check(Cmd.MarkFlagRequired("manifest"))

	Check(viper.BindPFlag("mountpoint", flags.Lookup("mountpoint")))

	Check(viper.BindPFlag("manifest", flags.Lookup("manifest")))
}

var Cmd = &cobra.Command{
	Use:           "fuse [OPTIONS]",
	Short:         "Mount a storage container as a normal filesystem",
	SilenceUsage:  true,
	SilenceErrors: true,
	//DisableFlagsInUseLine: true,
	//Args:                  cli.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {

		source := viper.GetViper()
		source.SetConfigFile(viper.GetString("manifest"))
		if err := source.ReadInConfig(); err != nil {
			return err
		}

		// Prepare the peer to the virtual infrastructure
		man, err := manifest.New(source)
		if err != nil {
			return err
		}

		// Mount the gateway
		conn, err := fuse.Mount(
			viper.GetString("mountpoint"),
			fuse.FSName("tromosfs"),
			fuse.Subtype("tromosfs"),
			fuse.AsyncRead(),
			//fuse.MaxReadahead(0xffffff),
			//fuse.WritebackCache(),
		)
		if err != nil {
			return err
		}
		defer func() {
			Check(conn.Close())
			Check(fuse.Unmount(viper.GetString("mountpoint")))
		}()

		client, err := middleware.NewClient(middleware.Config{Manifest: man})
		if err != nil {
			return err
		}
		defer client.Close()
		return tromosfs.Mount(tromosfs.FilesystemConfig{
			Conn:   conn,
			Client: client,
		})
	},
}
